$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var owl 	      = $(".owl-carousel"),
	testimonials  = $(".testimonials_slider"),
	swiper 		  = $(".swiper-container"),
	styler 		  = $(".styler"),
	tabs 	      = $("#parentHorizontalTab");

	if(owl.length || testimonials.length){
	  	include("js/owl.carousel.js");
	}
	if(tabs.length){
	  	include("js/easyResponsiveTabs.js");
	}
	if(swiper.length){
	  	include("js/swiper.js");
	}
	if(styler.length){
      include("js/formstyler.js");
    }


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

	if(styler.length){
      $(".styler").styler({});  
    }
      


	// responsive NAV
		$(".resp_menu_btn, .resp_menu_close").on('click ontouchstart', function(e){
	  		var body = $('body');
	  		
	  		body.toggleClass('nav_overlay');
	    })


    /* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){
			owl.owlCarousel({
				items : 1,
				navigation: true,
			    slideSpeed : 1000,
				responsive: true,
				itemsDesktop : [1199,1],
			    itemsDesktopSmall : [995,1],
			    itemsTablet: [768,1],
			    itemsMobile : [320,1]
			});
		}

		if(testimonials.length){
			testimonials.owlCarousel({
				items : 1,
				navigation: true,
			    slideSpeed : 1000,
			    responsive: true,
				itemsDesktop : [1199,1],
			    itemsDesktopSmall : [995,1],
			    itemsTablet: [768,1],
			    itemsMobile : [320,1]
			});
		}

	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */


	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if(tabs.length){
			tabs.easyResponsiveTabs({
				type: 'horizontal', 
		        width: 'auto', 
		        fit: true, 
		        closed: 'accordion', 
		        tabidentify: 'hor_1', 
			});
		}

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */


	/* ------------------------------------------------
	SWIPER START
	------------------------------------------------ */

		if(swiper.length){
			swiper.swiper({
				scrollbar: '.swiper-scrollbar',
		        scrollbarHide: true,
		        slidesPerView: 'auto',
		        centeredSlides: false,
		        spaceBetween: 30,
		        grabCursor: true,
		         breakpoints: {
			    // when window width is <= 320px
			    667: {
			      slidesPerView: 1,
			      spaceBetween: 10
			    },
			    768: {
			      slidesPerView: 2,
			      spaceBetween: 15
			    },
			    1460: {
			      slidesPerView: 3,
			      spaceBetween: 20
			    },
			    1640: {
			      slidesPerView: 4,
			      spaceBetween: 30
			    }
			  }
			});

		}

	/* ------------------------------------------------
	SWIPER END
	------------------------------------------------ */

})